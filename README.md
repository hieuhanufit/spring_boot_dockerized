# spring_boot_dockerized
## Organize Your Project:
```
project/
|-- Dockerfile
|-- docker-compose.yml
|-- src/
    |-- main/
        |-- resources/
            |-- application-dev.properties
            |-- application-staging.properties
            |-- application-production.properties
```

## Build
In your Spring Boot application, create multiple configuration files for each environment. For instance, application-dev.properties, application-staging.properties, and application-production.properties.
Build your Spring Boot application:
```
./mvnw clean package
```

# For dev environment
```
docker-compose -f docker-compose.yml -f docker-compose.dev.yml up
```
# For staging environment
```
docker-compose -f docker-compose.yml -f docker-compose.staging.yml up
```
# For production environment
```
docker-compose -f docker-compose.yml -f docker-compose.prod.yml up
```

